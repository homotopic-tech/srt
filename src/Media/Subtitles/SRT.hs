-- |
--   Module    : Media.Subtitles.SRT
--   License   : MIT
--   Stability : experimental
--
-- The SRT subtitle type.
module Media.Subtitles.SRT
  ( Line (..),
    SRT (..),
  )
where

import Data.Text
import Media.Timestamp

-- | Data type for a single line in SRT.
data Line = Line
  { index :: Int,
    range :: Range,
    dialog :: Text
  }
  deriving (Eq, Show, Ord)

-- | SRT data type.
--
-- @since 0.1.0.0
data SRT = SRT {unSRT :: [Line]}
  deriving (Eq, Show)
