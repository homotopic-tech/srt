# srt - Subtitle Data Type

The data type representing lines in the SRT subtitle format. This is just the
data type to avoid including any particular parser or formatter as a
dependency.
